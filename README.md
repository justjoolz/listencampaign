# Listen Auction House 
## Flow smart contracts, transactions and scripts.

Each Listen NFT is minted and stored in a collection
NFTs can be minted single or in batches.

NFTs are pre-minted and passed to ListenAuction contract that holds them in escrow until the settleAuction function is called.

ListenStoreFront is customized version of the onflow/NFTStoreFront contract

ListenUSD is a utility FT for bidding purposes. If ListenCampaign uses Ingenico as payment provider we will use this similar to DUC on NBATS.

If they choose MoonPay or Circle we may be able to use a native stable coin.


### ListenNFT
- mint batch collection
- mint nft
- setup account
- transfer nft

### ListenAuction
- create_auction from collection
- create_auction from ids
- create_auction
- place bid
- settle auction


### ListenStorefront
- buy item
- cleanup item
- remove item
- sell item
- setup account


### ListenUSD
- burn tokens
- mint tokens
- setup account
- transfer tokens
