import FungibleToken from "../../contracts/dependencies/FungibleToken.cdc"
import ListenToken from "../../contracts/ListenToken.cdc"

transaction(recipient: Address, amount: UFix64) {
    let tokenAdmin: &ListenToken.Administrator
    let tokenReceiver: &{FungibleToken.Receiver}

    prepare(signer: AuthAccount) {
        self.tokenAdmin = signer
        .borrow<&ListenToken.Administrator>(from: ListenToken.AdminStoragePath)
        ?? panic("Signer is not the token admin")

        self.tokenReceiver = getAccount(recipient)
        .getCapability(ListenToken.ReceiverPublicPath)
        .borrow<&{FungibleToken.Receiver}>()
        ?? panic("Unable to borrow receiver reference")
    }

    execute {
        let minter <- self.tokenAdmin.createNewMinter(allowedAmount: amount)
        let mintedVault <- minter.mintTokens(amount: amount)

        self.tokenReceiver.deposit(from: <-mintedVault)

        destroy minter
    }
}
