import FungibleToken from "../../contracts/dependencies/FungibleToken.cdc"
import ListenToken from "../../contracts/ListenToken.cdc"

// This transaction is a template for a transaction
// to add a Vault resource to their account
// so that they can use the ListenToken

transaction {

    prepare(signer: AuthAccount) {

        if signer.borrow<&ListenToken.Vault>(from: ListenToken.VaultStoragePath) == nil {
            // Create a new ListenToken Vault and put it in storage
            signer.save(<-ListenToken.createEmptyVault(), to: ListenToken.VaultStoragePath)

            // Create a public capability to the Vault that only exposes
            // the deposit function through the Receiver interface
            signer.link<&{FungibleToken.Receiver}>(
                ListenToken.ReceiverPublicPath,
                target: ListenToken.VaultStoragePath
            )

            // Create a public capability to the Vault that only exposes
            // the balance field through the Balance interface
            signer.link<&{FungibleToken.Balance}>(
                ListenToken.BalancePublicPath,
                target: ListenToken.VaultStoragePath
            )
        }
    }
}
