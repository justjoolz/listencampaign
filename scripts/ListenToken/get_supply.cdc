import ListenToken from "../../contracts/ListenToken.cdc"

// This script returns the total amount of ListenToken currently in existence.

pub fun main(): UFix64 {

    let supply = ListenToken.totalSupply

    log(supply)

    return supply
}
