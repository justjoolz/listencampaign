import FungibleToken from "../../contracts/dependencies/FungibleToken.cdc"
import ListenToken from "../../contracts/ListenToken.cdc"

// This script returns an account's ListenToken balance.

pub fun main(address: Address): UFix64 {
    let account = getAccount(address)
    
    let vaultRef = account.getCapability(ListenToken.BalancePublicPath).borrow<&{FungibleToken.Balance}>()
        ?? panic("Could not borrow Balance reference to the Vault")

    return vaultRef.balance
}
